import {
TestComponentBuilder,
describe,
expect,
inject,
it,
AsyncTestCompleter
} from 'angular2/testing_internal';

import {RegisterComponent} from './component';

export function main() {

  describe('RegisterComponent', () => {

    it('should work',
      inject([TestComponentBuilder, AsyncTestCompleter], (tcb: TestComponentBuilder, async: AsyncTestCompleter) => {
        tcb.createAsync(RegisterComponent).then((fixture) => {
          const compiled = fixture.debugElement.nativeElement;
          expect(compiled.querySelector('h2').textContent).toEqual('Home!');
          async.done();
        });
      }));

  });
}
