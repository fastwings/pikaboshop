import {Component} from 'angular2/core';
import { UserRegisterModel }    from './model';
@Component({
    selector: 'home',
    moduleId: __moduleName,
    templateUrl: 'template.html',
})
export class RegisterComponent {
    model = new UserRegisterModel("", "", "", "", 2, "", "", "", "");
    errors = []
    onSubmit() {
        this.errors = []
        this.errors = this.model.validate();
        if (this.errors.length != 0) {
            console.log("Error");
        }
        console.log(JSON.stringify(this.model));
    }

    get diagnostic() {
        return JSON.stringify(this.model);
    }
    get diagnosticError() {
        let errors = []
        errors = this.model.validate();
        return JSON.stringify(errors);
    }
}
