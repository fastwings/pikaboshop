export class UserRegisterModel {
    constructor(public fname:string,
                public lname:string,
                public country:string,
                public city:string,
                public timezone:number,
                public birthdate:string,
                public email:string,
                public password:string,
                public password_conf:string
    ) {}
    validate() {
        let errors = [];
        if (this.is_empty(this.fname)){
            let obj = {
                message: "First name field is Empty"
            };
            obj.message = "First name field is Empty";
            errors.push(obj);
        }
        if (this.is_empty(this.lname)){
            let obj = {
                message: "Last name field is Empty"
            };
            errors.push(obj);
        }
        if (this.is_empty(this.country)){
            let obj = {
                message: "Country field is Empty"
            };
            errors.push(obj);
        }
        if (this.is_empty(this.city)){
            let obj = {
                message: "City field is Empty"
            };
            errors.push(obj);
        }
        if (this.is_empty(this.birthdate)){
            let obj = {
                message: "Date is Empty"
            };
            errors.push(obj);
        }
        if (this.is_empty(this.email)){
            let obj = {
                message: "Email is Empty"
            };
            errors.push(obj);
        }
        if (this.is_empty(this.password)|| this.is_empty(this.password_conf)){
            let obj = {
                message: "Password is Empty"
            };
            errors.push(obj);
        }
        if (this.password != this.password_conf){
            let obj = {
                message: "Password is not Match"
            };
            errors.push(obj);
        }
        return errors;
    }
    is_empty(value) {
        return (value == "");
    }
}
