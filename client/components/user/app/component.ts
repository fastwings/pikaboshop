import {Component, ViewEncapsulation} from 'angular2/core';
import {
    RouteConfig,
    ROUTER_DIRECTIVES
} from 'angular2/router';

import {LoginComponent} from '../login/component';
import {RegisterComponent} from '../register/component';
import {HttpUtil} from '../../../utils/http.util';
import {Notification} from '../../../models/dto';


@Component({
    selector: 'app',
    moduleId: __moduleName,
    templateUrl: 'template.html',
    directives: [ROUTER_DIRECTIVES],
    encapsulation: ViewEncapsulation.None
})
@RouteConfig([
    { path: '/login', component: LoginComponent, as: 'Login', useAsDefault: true },
    { path: '/register', component: RegisterComponent, as: 'Contact' }
])
export class UserAppComponent {

    loading: boolean;

    constructor(private httpUtil: HttpUtil) {

        let numReqStarted = 0;
        let numReqCompleted = numReqStarted;

        this.httpUtil.requestNotifier.subscribe((notification: Notification) => {

            if (notification.type === 'start') {
                ++numReqStarted;
            } else if (notification.type === 'complete') {
                ++numReqCompleted;
            }

            this.loading = numReqStarted > numReqCompleted;
        });
    }
}
