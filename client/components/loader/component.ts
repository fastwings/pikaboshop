import {COMMON_DIRECTIVES, COMMON_PIPES} from 'angular2/common';
import {Component, Input} from 'angular2/core';


@Component({
  selector: 'contact',
  moduleId: __moduleName,
  templateUrl: 'template.html',
  directives: [],
  pipes: [],
  providers: []
})
export class LoaderComponent {
  @Input()
  load: boolean;
}
