import * as yargs from 'yargs';
import * as slash from 'slash';
import * as fse from 'fs-extra';
import * as fs from 'fs';
import * as yaml from 'js-yaml';
const argv = yargs.argv;
const CWD = slash(process.cwd());

process.env.APP_ENVIRONMENT = argv['env'] || process.env.APP_ENVIRONMENT || 'dev';
export const IS_PROD = (process.env.APP_ENVIRONMENT === 'prod' || process.env.APP_ENVIRONMENT === 'staging');
export const IS_DEV = (process.env.APP_ENVIRONMENT === 'dev' || process.env.APP_ENVIRONMENT === 'testing');

export const PORT = 5555;
export const LIVE_RELOAD_PORT = 4002;
export const APP_ROOT = '/';
export const Log_Folder = 'C:\\Users\\654\\OneDrive\\Documents\\Stream Projects\\pikaboshop\\logs';
const TMP = 'tmp';
const CLIENT_SRC = 'client';
const CLIENT_UPLOADED = 'uploaded';
const ASSETS_SRC = 'assets';
const CLIENT_DEST = 'dist';
const NM = 'node_modules';
const INDEX_HTML = `${CLIENT_SRC}/index.html`;
const USER_HTML = `${CLIENT_SRC}/user.html`;
const TS_LIB_DEF = [
  'typings/main/ambient/es6-shim/*.d.ts',
  'tools/manual_typings/module.d.ts'
];
export const configs = {};
//we need load the all basic config for connections
fs.readdirSync(__dirname + '/configs').forEach(function(file) {
  if (file.match(/\.yml$/) !== null) {
    let name = file.replace(".yml",'');
    let config = yaml.safeLoad(fs.readFileSync(__dirname + '/configs/'+file, 'utf8'));
    configs[name] = config;
  }
});

export const PATHS = {
  cwd: CWD,
  src: {
    vendor: {
      js: [
        `${NM}/es6-shim/es6-shim.js`,
        `${NM}/systemjs/dist/system-register-only.src.js`,
        `${NM}/rxjs/bundles/Rx.js`,
        `${NM}/angular2/bundles/angular2-polyfills.js`,
        `${NM}/angular2/bundles/angular2.dev.js`,
        `${NM}/angular2/bundles/router.dev.js`,
        `${NM}/angular2/bundles/http.dev.js`,
      ],
      copyOnly: [
        `${NM}/systemjs/dist/system-polyfills.src.js`,
        `${NM}/bootstrap/dist/css/bootstrap.css.map`
      ],
      font: [
        `${NM}/bootstrap/dist/fonts/*`
      ],
      css: [
        `${NM}/bootstrap/dist/css/bootstrap.css`
      ]
    },
    custom: {
      index: INDEX_HTML,
      user: USER_HTML,
      uploaded: CLIENT_UPLOADED,
      tpl: [
        `${CLIENT_SRC}/**/*.html`,
        `!${INDEX_HTML}`
      ],
      assets: [
        `${ASSETS_SRC}/fonts/**/*.svg`,
        `${ASSETS_SRC}/images/**/*.png`,
        `${ASSETS_SRC}/images/**/*.jpg`,
        `${ASSETS_SRC}/images/**/*.gif`,
        `${ASSETS_SRC}/fonts/**/*.eot`,
        `${ASSETS_SRC}/fonts/**/*.ttf`,
        `${ASSETS_SRC}/fonts/**/*.woff`,
        `${ASSETS_SRC}/fonts/**/*.woff2`
      ],
      css: `${CLIENT_SRC}/**/*.css`,
      less: `${CLIENT_SRC}/**/*.less`,
      tsApp: TS_LIB_DEF.concat([
        `${CLIENT_SRC}/**/*.ts`,
        `!${CLIENT_SRC}/**/spec.ts`
      ]),
      tsLint: [
        `gulpfile.ts`,
        `tools/**/*.ts`,
        `${CLIENT_SRC}/**/*.ts`,
        `server/**/*.ts`,
        '!tools/manual_typings/**'
      ],
      test: TS_LIB_DEF.concat([
        'typings/main/ambient/jasmine/*.d.ts',
        `${CLIENT_SRC}/**/spec.ts`
      ])
    }
  },
  dest: {
    tmp: TMP,
    test: `${TMP}/test`,
    coverage: `${TMP}/coverage`,
    dist: {
      base: CLIENT_DEST,
      css: `${CLIENT_DEST}/assets/css`,
      appBundle: `${CLIENT_DEST}/assets/javascript/app.bundle.js`,
      lib: `${CLIENT_DEST}/assets/lib`,
      assets: `${CLIENT_DEST}/assets`,
      font: `${CLIENT_DEST}/assets/fonts`
    }
  }
};


const TSC_OPTS = fse.readJsonSync(`${CWD}/tsconfig.json`).compilerOptions;

// Setting the following options here since them cause issues on the VSC IDE.
// TODO: move to tsconfig.json once IDE adds support for them.
TSC_OPTS.forceConsistentCasingInFileNames = true;
TSC_OPTS.pretty = true;
TSC_OPTS.module = 'system';

export const TSC_APP_OPTS = Object.assign({}, TSC_OPTS, {
  outFile: PATHS.dest.dist.appBundle
});

export const TSC_TEST_OPTS = Object.assign({}, TSC_OPTS, {
  outDir: PATHS.dest.test
});

console.log('process.env.APP_ENVIRONMENT: ', process.env.APP_ENVIRONMENT);
