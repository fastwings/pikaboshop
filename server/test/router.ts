import * as express from 'express';
import {Test} from '../entities/TestModel';
import {configs} from '../../tools/config';
// import {sendError} from '../utils/web.util';
const router = express.Router();

router.get('/add', (req:express.Request, res:express.Response) => {
    let t = new Test({name: 'small', somethingElse: 3});
    t.save((err)=> {
        //if (err) return handleError(err);
    });
    let obj = {
        data: t,
        config: configs['test'][process.env.APP_ENVIRONMENT]
    };
    res.json(obj);
});
/*
 router.post('/', (req, res) => {
 contactService.createOne(req.body)
 .then((contact: Contact) => res.send(contact), (err: any) => sendError(res, err));
 });

 router.put('/:id', (req, res) => {
 contactService.updateOne(req.body)
 .then((contact: Contact) => res.send(contact), (err: any) => sendError(res, err));
 });

 router.delete('/:id', (req, res) => {
 contactService.removeOneById(req.params.id)
 .then((contact: Contact) => res.send(contact), (err: any) => sendError(res, err));
 });

 router.get('/_find', (req: express.Request, res: express.Response) => {
 contactService.find()
 .then((contacts: Contact[]) => res.send(contacts), (err: any) => sendError(res, err));
 });

 router.get('/:id', (req: express.Request, res: express.Response) => {
 contactService.findOneById(req.params.id)
 .then((contact: Contact) => res.send(contact), (err: any) => sendError(res, err));
 });
 */
export = router;
