import * as express from 'express';
import {Test} from '../entities/TestModel';
import {configs} from '../../tools/config';
module.exports  = function (io) {
    var broadcast = io.of('/broadcast')
    broadcast.on('connection', function (socket) {
        socket.emit('foo', 'bar')
    })
}
