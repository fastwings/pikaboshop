import mongoose = require('mongoose');
import {IUserProfile} from './UserProfileModel';
let bcrypt   = require('bcrypt-nodejs');

interface IUserProvider extends mongoose.Document {
    local: IUserLocal;
    facebook: IUserFB;
    twitter: IUserTwitter;
    google: IUserGoogle;
    profile: IUserProfile;
};
interface IUserLocal extends mongoose.Document {
    email: string;
    password: string;
};
interface IUserFB extends mongoose.Document {
    id: string;
    token: string;
    email: string;
    name: string;
};
interface IUserTwitter extends mongoose.Document {
    id: string;
    token: string;
    displayName: string;
    username: string;
};
interface IUserGoogle extends mongoose.Document {
    id: string;
    token: string;
    email: string;
    name: string;
};
let userSchema = new mongoose.Schema({
    local            : {
        email        : String,
        password     : String,
    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }
});
// create the model for users and expose it to our app
export const User = mongoose.model<IUserProvider>('User', userSchema);
