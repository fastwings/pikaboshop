import mongoose = require('mongoose');

export interface IUserProfile extends mongoose.Document {
    fname: string;
    lname: string;
    city: string;
    country: string;
    timezone: number;
    birthday: Date;
};

const UserProfileSchema = new mongoose.Schema({
    fname: {type:String, required: true},
    lname: {type:String, required: true},
    city: {type:String, required: true},
    country: {type:String, required: true},
    birthday: {type:Number, required: true},
    timezone: {type:Date, required: true}
});

export const UserProfile = mongoose.model<IUserProfile>('UserProfile', UserProfileSchema);

