import mongoose = require('mongoose');

interface ITest extends mongoose.Document {
    name: string;
    somethingElse?: number;
};

const UserSchema = new mongoose.Schema({
    name: {type:String, required: true},
    somethingElse: Number,
});

export const Test = mongoose.model<ITest>('Test', UserSchema);
