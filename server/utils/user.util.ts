/**
 * Created by 654 on 6/18/2016.
 */
import * as bcrypt from 'bcrypt-nodejs';
// methods ======================
// generating a hash
//noinspection TypeScriptUnresolvedVariable
export const generateHash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8)).toString();
};

// checking if password is valid
//noinspection TypeScriptUnresolvedVariable
export const validPassword = (password, localPassword) => {
    return bcrypt.compareSync(password, localPassword);
};
