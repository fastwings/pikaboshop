import * as connectLivereload from 'connect-livereload';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as fs from 'fs';
import {join} from 'path';
import {resolve} from 'path';
import * as mongoose from 'mongoose';
import * as session from 'express-session';
import * as connectMongo from 'connect-mongo';
import {LIVE_RELOAD_PORT, PATHS, PORT, APP_ROOT,Log_Folder,configs,IS_PROD,IS_DEV} from '../tools/config';
import * as contactRouter from './contact/router';
import * as testRouter from './test/router';
import * as testSocketsRouter from './test/sockets';
import * as morgan from 'morgan';
import * as testSocket from './test/sockets';
import flash = require('connect-flash');
import passport = require('passport');
import cookieParser = require('cookie-parser');
import {handleError} from './utils/web.util';
let FileStreamRotator = require('file-stream-rotator');
import errorhandler = require('errorhandler');
import notifier = require('node-notifier');
import redis = require('socket.io-redis');
// instantiate PrettyError, which can then be used to render error objects
let MongoStore = connectMongo(session);
const INDEX_DEST_PATH = resolve(PATHS.cwd, PATHS.dest.dist.base, 'index.html');
const USER_DEST_PATH = resolve(PATHS.cwd, PATHS.dest.dist.base, 'user.html');

const server = express();

let logDirectory = Log_Folder;
function errorHandler(err, req, res, next) {
    if (err) {
        console.log(err);
    }
    next();
}

function errorNotification(err, str, req) {
    var title = 'Error in ' + req.method + ' ' + req.url

    notifier.notify({
        title: title,
        message: str
    })
}
process.on('unhandledRejection', (reason)=> {
    console.log("Critical Error");
    console.log(reason);
});
// we can optionally configure prettyError to simplify the stack trace:

mongoose.connect(configs['db'][process.env.APP_ENVIRONMENT].uri);
// ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
// create a rotating write stream
let accessLogStream = FileStreamRotator.getStream({
    date_format: 'YYYYMMDD',
    filename: join(logDirectory, 'access-%DATE%.log'),
    frequency: 'daily',
    verbose: false
});
// and use it for our app's error handler:
server.use(errorHandler);
if (IS_DEV) {
    // only use in development
    server.use(errorhandler({log: errorNotification}))
}
server.use(session({
    secret: configs['db'][process.env.APP_ENVIRONMENT].session_key,
    store: new MongoStore({mongooseConnection: mongoose.connection})
}));
console.log("Configs:", configs);
// setup the logger
if (IS_DEV) {
    server.use(morgan('dev', {stream: accessLogStream}));//make sure we get all the text for better debug
}
else {
    server.use(morgan('combined', {stream: accessLogStream}));//prod or stating will get the apache style node it also ever env u setup as long it not dev
}
// setup the logger
server.use(APP_ROOT, <any> connectLivereload({port: LIVE_RELOAD_PORT}));
server.use(express.static(PATHS.dest.dist.base));//combile folder no need add stuff here
server.use(bodyParser.json());
server.use(flash());
server.use(cookieParser());
server.use(passport.initialize());
server.use(bodyParser.urlencoded({extended: false}));
server.get('/api/**', (req, res, next) => {
    // TODO: remove this. It just mimics a delay in the backend.
    const delay = Math.floor((Math.random() * 300) + 1);
    setTimeout(() => next(), delay);
});
export let Payload_Data = {};
server.use('/api/contact', contactRouter);
server.use('/api/test', testRouter);
server.use('/api/error', () => {
    new Error("Test");
});

server.get(`${APP_ROOT}/dologin`, passport.authenticate('local-login', {
    failWithError: true
}, function (err, req, res, next) {
    // handle success
    if (req.xhr) {
        if (!err) {
            return res.json({id: req.user.id});
        }
        else {
            return handleError(err, req, res, next)
        }
    }
    return res.redirect('/');
}));
server.get(`${APP_ROOT}/doregister`, passport.authenticate('local-signup', {
    failWithError: true
}, function (err, req, res, next) {
    // handle success
    if (req.xhr) {
        if (!err) {
            return res.json({id: req.user.id});
        }
        else {
            return handleError(err, req, res, next)
        }
    }
    return res.redirect('/');
}));
server.get(`${APP_ROOT}login`, (req, res) => {
    res.sendFile(USER_DEST_PATH);
});
server.get(`${APP_ROOT}register`, (req, res) => {
    res.sendFile(USER_DEST_PATH);
});
server.get(`${APP_ROOT}`, (req, res) => {
    res.sendFile(INDEX_DEST_PATH);
});
server.listen(PORT);

let socket_server = require('http').Server(server);
let io = require('socket.io')(socket_server);
io.adapter(redis(configs['redis'][process.env.APP_ENVIRONMENT].uri));
//all web sockets must be added here

console.log(testSocket);
//let io_files = [
//    testSocket(io)
//];
//io_files.forEach(function (socketIo) {
//    //noinspection TypeScriptUnresolvedFunction
//    socketIo.attach(server);
//});
io.on('connection', (socket)=> {
    console.log("Connected");
})
